import 'dart:async';
import 'package:flutter_myapi/bookdetail.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'dart:convert';


class Book{
 final String description;
 final String imagePath;

 Book({this.description, this.imagePath});


}


void main() => runApp(new MaterialApp(
  debugShowCheckedModeBanner: false,
  home: new  HomePage(),
));

class HomePage extends StatefulWidget{

  @override
  HomePageState createState()=> new HomePageState();
}

class HomePageState extends State<HomePage>{

  final String url = "https://www.googleapis.com/books/v1/volumes?filter=free-ebooks&q=a";
  List data;
//  String  detail,image;


  @override
  void initState() {
    super.initState();
    this.getJsonData();
  }

  Future<String> getJsonData() async{
    var response = await http.get(
      Uri.encodeFull(url),
      headers: {"Accept": "application/json"}
    );
    print(response.body);

    setState(() {
      var convertDataToJson = jsonDecode(response.body);
      data = convertDataToJson['items'];

    });
    return "Success";
  }

  @override
  Widget build(BuildContext context){
    return new Scaffold(

      appBar: new AppBar(
        title: new Text("BooksApi"),
      ),

      body: new ListView.builder(
          itemCount: data == null ? 0 : data.length,
          itemBuilder: (BuildContext context,int index){
            return new Container(

              child: new Center(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    new Card(
                      child :InkWell(
                        child: new Container(
                          child: new Text(data[index]['volumeInfo']['title']),
                          padding: const EdgeInsets.all(20.0),
                        ),
                        onTap: () {
                         var  detail = data[index]['volumeInfo']['subtitle'];
                         var  image = data[index]['volumeInfo']['imageLinks']['thumbnail'];
                         print(detail);
                         print(image);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => BookDetial(
                                  pass:Book(
                                    description: detail,
                                    imagePath: image,
                                  ),
                                )),
                          );
                           },

                      ),

                    ),
                  ],
                ),
              ),
            );

          }
      ),
    );
  }

}



import 'package:flutter/material.dart';
import 'package:flutter_myapi/main.dart';

class BookDetial extends StatefulWidget{
  final Book pass;

  BookDetial({this.pass});

  @override
  BookDetialState createState() => BookDetialState();

}

class BookDetialState extends State<BookDetial> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('Detailscreen')),
      body: new Container(
        child: new Center(
          child: Column(
            children: <Widget>[
              Padding(
                child: new Text(
                  'Subtitle : ${widget.pass.description}',
                  style: new TextStyle(fontWeight: FontWeight.bold),
                  textAlign: TextAlign.left,
                ),
                padding: EdgeInsets.all(10.0),
              ),
              Padding(
                child: Image.network( '${widget.pass.imagePath}'),
                padding: EdgeInsets.only(bottom: 8.0),
              ),
            ],
          ),
        ),
      ),
    );
  }
}